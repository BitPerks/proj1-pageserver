# README #
10.15.19
A "getting started" project for CIS 322, introduction to software engineering, at the University of Oregon.

### What is it? ###
This is a basic HTTP server run through Python to be used in CIS 322 Project 1. The server will take a URL and 
respond with an error message or with a containing file.

Repo Also contains scripts used for quick testing and a basic HTML and CSS file to run properly through the HTTP Server.
 

### Author ###
Isaac Perks, iperks@uoregon.edu
Server/Repo fork Provided and maintained by: Ram Durairajan, Steven Walton

